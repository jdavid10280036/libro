/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;
import javax.persistence.*;
import entidad.Libro;
import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.Stateless;
/**
 *
 * @author fer
 */
@Stateless
public class CatalogoBean implements CatalogoBeanRemote{
    @PersistenceContext(unitName="libroo-ejbPU")
    EntityManager em;
    protected Libro libro;
    protected Collection <Libro> ListaLibros;
    
    public void addLibro(java.lang.String titulo,java.lang.String autor,BigDecimal precio){
        if(libro==null){
            libro=new Libro(titulo,autor,precio);
        }
        em.persist(libro);
    }
    
    public Collection<Libro> getAllLibros(){
        ListaLibros=em.createNamedQuery("Libro.findAll").getResultList();
        return ListaLibros;
    }
        public Libro buscaLibro(int id)
    {
        libro=em.find(Libro.class, id);
        /*
        System.out.println("Regresando Libro");
        if(libro!=null)
            System.out.println("Libro hallado "+libro.getTitulo());
        else
            System.out.println("Libro no hallado");
        */
        return libro;
    }
           public void actualizaLibro(Libro libro, String Titulo, String autor, BigDecimal precio)
    {
        if(libro!=null)
        {
            System.out.println("Actualizando libro");
            libro.setTitulo(Titulo);
            libro.setAutor(autor);
            libro.setPrecio(precio);
            em.merge(libro);
        }
    }
              public void eliminaLibro(int id)
    {
         libro=em.find(Libro.class, id);
       
        {
            System.out.println("Eliminando Registro");
          
            em.remove(libro);
            libro=null;
            
        }
    }

}
